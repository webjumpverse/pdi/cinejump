import { getObjectValue } from './objects'

export function isFavorite(list:any, item:any) {
  return list.some((el:any) => getObjectValue(el, 'id') === item.id)
}

export function setFavorites(favorites:any[], item:any) {
  return favorites.map((fav:any) => ({ ...fav, farorite: fav.id === item.id ? true : false }))
}