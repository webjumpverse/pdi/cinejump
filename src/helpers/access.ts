export function getLoggedUser(key:string) {
  const user = sessionStorage.getItem(key);
  return user
}

export function registeredUser(user:object) {
  const registration = localStorage.getItem('registration')
  const parsedRegistration = registration?.length ? JSON.parse(registration) : []
  const isRegistered = parsedRegistration.find((register:any) => register.email === user.email)
  
  return {
    isRegistered,
    parsedRegistration
  }
}