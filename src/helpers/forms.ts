export function handleSignUp(data:object, reset:Function, registered:object[], exists:object) {
  const { name, email, password } = data

  if (!name || !email || !password || exists) {
    return false
  } else {
    localStorage.setItem('registration', JSON.stringify([...registered, data]))
    reset()
    return true
  }
}