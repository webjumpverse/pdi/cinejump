import axios from 'axios'

const endPoint = axios.create({
  baseURL: `${import.meta.env.VITE_BASE_URL}`
})

async function getMovies(id:string|number) {
  const movies = await endPoint
    .get(`movie/${id}?api_key=${import.meta.env.VITE_TMDB_API_KEY}&language=pt-BR&page=1&region=BR`)
    .then(movies => movies.data.results)
    .catch(error => console.error('getMovies service', error))
  return movies;
}

async function getMoviesTrailers(movies:any) {
  const trailers = await axios
    .all(movies.map((movie: { id:number }) => endPoint
      .get(`movie/${movie.id}/videos?api_key=${import.meta.env.VITE_TMDB_API_KEY}&language=pt-BR&page=1&region=BR`)
      .then(trailers => {
        const { data } = trailers;
        return data.results[0];
      })
      .catch(error => console.error(error))
    ))
    .then(trailers => trailers.filter((trailer) => trailer !== undefined))
    .catch(error => console.error('getMoviesTrailers service', error))
  return trailers
}

async function getInfosById(context:string, id:string) {
  const movie = await endPoint
    .get(`${context}/${id}?api_key=${import.meta.env.VITE_TMDB_API_KEY}&language=pt-BR&page=1&region=BR&append_to_response=videos,images,recommendations,keywords,credits`)
    .then(movie => movie.data)
    .catch(error => console.error('getMovieById service', error))
  return movie
}

export {
  getMovies,
  getMoviesTrailers,
  getInfosById
}