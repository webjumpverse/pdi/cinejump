import styled from 'styled-components'

import { Header } from './header'

import { DESIGN_SYSTEM as DS } from '../ds'
import { useAuthorization } from '../../contexts';

export interface HeaderProps {
  background?: string;
  page: string;
}

const HeaderBg = styled.div<HeaderProps>`
  &.movie {
    background-color: ${DS.COLORS.primary};
    height: 587px;
    position: absolute;
    width: 100%;
    z-index: -1;

    &::after {
      background-image: url(${props => props.background});
      background-position: center center;
      background-repeat: no-repeat;
      background-size: cover;
      content: '';
      display: block;
      height: 100%;
      mix-blend-mode: overlay;
      position: absolute;
      width: 100%;
    }
  }

  &.person {
    background-color: ${DS.COLORS.secondary};
    height: 242px;
    position: absolute;
    width: 100%;
    z-index: -1;    
  }
  
  &.profile {
    background-color: ${DS.COLORS.secondary};
    height: 103px;
    position: absolute;
    width: 100%;
    z-index: -1;
  }
`;

export function CommonHeader({ page, background }:HeaderProps) {
  const { user } = useAuthorization()
  const headerBgClass:any = {
    favorites: 'favorites',
    home: 'home',
    movie: 'movie',
    person: 'person',
    profile: 'profile',
  }
  return (
    <>
      <HeaderBg className={headerBgClass[page]} background={`${import.meta.env.VITE_BASE_IMAGE_URL}${background}`} />
      <Header page={page} />
    </>
  )
}