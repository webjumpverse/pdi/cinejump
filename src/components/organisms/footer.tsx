import { useLocation } from 'react-router-dom';
import styled from 'styled-components'

import { ReactComponent as WhiteLogo } from '../../assets/imgs/WhiteLogo.svg'

import { Anchor } from '../atoms/anchor'

const CustomFooterContent = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;

  @media screen and (min-width:1024px) {
    display: grid;
    grid-template-columns: 50% 50%;
    padding: 2rem;
  }

  .logoContainer {
    align-items: center;
    display: flex;
    justify-content: center;
    margin-bottom: 1rem;

    @media screen and (min-width:1024px) {
      margin-bottom: unset;
    }
  }

  a {
    text-align: center;

    @media screen and (min-width:1024px) {
      text-align: unset;
    }
  }
`;

export function Footer() {
  const { pathname } = useLocation()
  const isProfile = pathname.includes('profile');

  return (
    <footer className={isProfile ? 'bg-secondary' : 'bg-primary'}>
      <CustomFooterContent>
        <div className='logoContainer'>
          <WhiteLogo />
        </div>
        <div>
          <Anchor to='#'>Desenvolvido por Lucas Gabriel</Anchor>
          <Anchor to='#'>Proposta do projeto</Anchor>
          <Anchor to='#'>Protótipo no Figma</Anchor>
          <Anchor to='#'>Apresentação ao comitê</Anchor>
          <Anchor to='#'>Documentação</Anchor>
        </div>
      </CustomFooterContent>
    </footer>
  )
}