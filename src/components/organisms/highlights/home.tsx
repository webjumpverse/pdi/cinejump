import styled from 'styled-components'

import { Image } from '../../atoms/image'
import { ImageLegend } from '../../molecules/image-legend'
import { Link } from 'react-router-dom';

const HomeHighlightsContent = styled.div`
  display: grid;
  gap: 1rem;
  grid-template-areas:
    'prime'
    'second'
    'third';
  
  @media screen and (min-width:1024px) {
    grid-template-areas:
      'prime second'
      'prime third';
  }

  a {
    grid-area: second;

    &:first-child {
      grid-area: prime;
    }
  
    &:last-child {
      grid-area: third;
    }
  }
`;

const HomeHighlightPrime = styled(Image)`
  
`;

const HomeHighlightSecond = styled(Image)`
`;
const HomeHighlightThird = styled(Image)`
`;

export function HomeHighlights({ data }:any) {
  const isMobile = globalThis.innerWidth < 1024
  return (
    <section className='container'>
      <HomeHighlightsContent>
        <Link to={`/movie/${data[0]?.id}`}>
          <HomeHighlightPrime format={isMobile ? 'responsive' : 'prime'} source={!data ? undefined : `${import.meta.env.VITE_BASE_IMAGE_URL}${data[0]?.backdrop_path}`}>
            <ImageLegend isPrime variation title={!data ? undefined : data[0]?.title} caption={!data ? undefined : data[0]?.overview} />
          </HomeHighlightPrime>
        </Link>
        <Link to={`/movie/${data[1]?.id}`}>
          <HomeHighlightSecond format={isMobile ? 'responsive' : 'second'}  source={!data ? undefined : `${import.meta.env.VITE_BASE_IMAGE_URL}${data[1]?.backdrop_path}`}>
            <ImageLegend variation caption={!data ? undefined : data[1]?.title} />
          </HomeHighlightSecond>
        </Link>
        <Link to={`/movie/${data[2]?.id}`}>
          <HomeHighlightThird format={isMobile ? 'responsive' : 'second'}  source={!data ? undefined : `${import.meta.env.VITE_BASE_IMAGE_URL}${data[2]?.backdrop_path}`}>
            <ImageLegend variation caption={!data ? undefined : data[2]?.title} />
          </HomeHighlightThird>
        </Link>
      </HomeHighlightsContent>
    </section>
  )
}