import styled from 'styled-components'

import { Image } from '../../atoms/image'
import { Title } from '../../atoms/title'
import { FavoriteMovie } from '../../molecules/favorite-movie'
import { SocialMedias } from '../../molecules/social-medias'

import { DESIGN_SYSTEM as DS } from '../../ds'

interface OtherHighlightsContentProps {
  data:any;
  isPerson?: boolean;
}

const OthersHightlightsContainer = styled.section`
  display: grid;
  gap: 2rem 0;
  /* grid-template-columns: auto auto; */
  
  @media screen and (min-width:1024px) {
    gap: 0 2rem;
    grid-template-columns: 294px auto;
  }

  .infos {
    padding: 0 ${DS.PADDINGS.regular};
  
    @media screen and (min-width:1024px) {
      padding: unset;
    }
  }  
`;

const OthersHightlightsPoster = styled.div`
  margin: 0 auto;
  position: relative;
  width: fit-content;
`;

const OtherHighlightsTitle = styled.div`
  color: ${DS.COLORS.lightinputbackgroung};
  margin-bottom: 2rem;
  
  @media screen and (min-width:1024px) {
    color: ${DS.COLORS.light};
  }

  h1 {
    margin-bottom: ${DS.MARGINS.small} !important;
  }
  
  p {
    color: inherit;
  }
`;

const OtherHighlightsTagline = styled.span`
  color: ${DS.COLORS.lighttext};
  display: block;
  font-style: italic;
  font-weight: ${DS.FONTS.weight.thin};
  
  @media screen and (min-width:1024px) {
    color: ${DS.COLORS.light};
  }
`;

const OtherHighlightsContent = styled.div<OtherHighlightsContentProps>`
  padding-top: 2rem;

  h3 {
    margin-bottom: 1rem !important;
  }

  p {
    color: ${DS.COLORS.lighttext};
    font-weight: ${DS.FONTS.weight.thin};
    line-height: 28px;
    letter-spacing: 0em;
  
    @media screen and (min-width:1024px) {
      color: ${props => (props.isPerson ? DS.COLORS.lighttext : DS.COLORS.light)};
    }
  }
`;

const OtherHighlightsDetail = styled.p`
  color: ${DS.COLORS.lighttext};
  margin-top: 3rem !important;
  
  @media screen and (min-width:1024px) {
    color: ${DS.COLORS.light};
  }

  span {
    font-weight: ${DS.FONTS.weight['semi-bold']};
  }
`;

export function OthersHighlight({ data, isPerson = true }:OtherHighlightsContentProps) {
  const isMobile = globalThis.innerWidth < 1024;
  const {
    birthday,
    biography,
    credits,
    gender,
    genres,
    id,
    name,
    overview,
    place_of_birth,
    profile_path,
    release_date,
    runtime,
    tagline,
    title,
    vote_average
  } = data;

  const dates = birthday ?? release_date;

  return (
    <OthersHightlightsContainer className='container vertical-padding'>
      <OthersHightlightsPoster id={isPerson ? `person-id-${id}` : `movie-id-${id}`}>
        {isPerson
          ? <Image source={`${import.meta.env.VITE_BASE_IMAGE_URL}${profile_path}`} format='profile' />
          : <FavoriteMovie movie={data} format='profile' />
        }
      </OthersHightlightsPoster>
      <div className='infos'>
        <OtherHighlightsTitle>
          <Title variation={isMobile ? 'lighttext' : 'light'}>{isPerson ? name : title}</Title>
          <p>{dates.split('-').reverse().join('/')} {isPerson && `- ${gender === 1 ? 'Feminino' : 'Masculino'}`} - {isPerson ? place_of_birth : genres.map((genre:any) => genre.name)}{!isPerson && ` - ${Math.floor(runtime / 60)}h${runtime % 60}min`}</p>
        </OtherHighlightsTitle>
        {!isPerson && tagline && <OtherHighlightsTagline>{tagline}</OtherHighlightsTagline>}
        {isPerson && <SocialMedias data={{}} />}
        <OtherHighlightsContent isPerson={isPerson}>
          <Title type='h3' variation={isMobile ? 'lighttext' : isPerson ? 'lighttext' : 'light'}>{isPerson ? 'Biografia' : 'Sinopse'}</Title>
          <p>{isPerson ? biography : overview}</p>
        </OtherHighlightsContent>
        {!isPerson && <OtherHighlightsDetail><span>Avaliação do Público:</span> {vote_average}</OtherHighlightsDetail>}
        {!isPerson && <OtherHighlightsDetail><span>Diretor:</span> {credits.crew.map((crew:any) => crew.department == 'Directing' && crew.name).filter((el:any) => el !== false).join(', ')}</OtherHighlightsDetail>}
      </div>
    </OthersHightlightsContainer>
  )
}