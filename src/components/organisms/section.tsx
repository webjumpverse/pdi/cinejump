import styled from 'styled-components'

import { Title } from '../atoms/title'

const SectionContent = styled.div`
  display: grid;
  gap: 1rem 0;
  
  .gallery {
    display: flex;
    gap: 0 1rem;
    overflow: auto hidden;
    touch-action: pan-x;
  }
`;

export function Section({ title, variation = 'primary', grid = true, children }:any) {
  const isTrailers = title == 'Trailers';
  
  return (
    <section className={`${isTrailers ? 'bg-secondary ' : ''}vertical-padding`}>
      <SectionContent className='container'>
        <Title type='h2' variation={variation}>{title}</Title>
        <div className={grid ? 'gallery' : ''}>{children}</div>
      </SectionContent>
    </section>
  )
}