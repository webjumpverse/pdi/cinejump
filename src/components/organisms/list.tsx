import styled from 'styled-components'

import { ListItems } from '../molecules/list-item'

import { DESIGN_SYSTEM as DS } from '../ds'

const CustomList = styled.ul`
  color: ${DS.COLORS.light};
`;

export function List(props: { credits:any }) {
  return (
    <CustomList>
      <ListItems items={props.credits} />
    </CustomList>
  )
}