import styled from 'styled-components'

import { FavoriteMovie } from '../molecules/favorite-movie'
import { Details } from '../molecules/details'

const FavoriteListContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem 0;
  padding: 1rem 0 0;
  
  @media screen and (min-width:768px) {
    flex-direction: unset;
    gap: 0 1rem;
  }
  
  &:not(:last-child) {
    border-bottom: 1px solid #EFEFEF;
    padding: 1rem 0;
  }
`;

const FavoritesListContent = styled.div`
  margin: 0 auto;
  /* position: relative; */
  width: fit-content;
`;

export function FavoritesList({ movie }:any) {
  return (
    <FavoriteListContainer>
      <FavoritesListContent>
        <FavoriteMovie movie={movie} format='poster' />
      </FavoritesListContent>
      <Details movie={movie} />
    </FavoriteListContainer>
  )
}