import { Menu } from '../molecules/menu'

import { HeaderProps } from './common-header'

export function Header({ page }:HeaderProps) {
  return (
    <header className={'vertical-padding'}>
      <Menu page={page} />
    </header>
  )
}