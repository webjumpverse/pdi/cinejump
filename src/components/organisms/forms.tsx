import { useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { Form } from '@unform/web'

import styled from 'styled-components'

import { Button } from '../atoms/button'
import { FormField } from '../molecules/form-field'
import { useAuthorization } from '../../contexts'
import { handleSignUp } from '../../helpers/forms'
import { registeredUser } from '../../helpers/access';

const CustomForm = styled(Form)`
  max-width: 530px;
  width: 100%;

  fieldset {
    margin-bottom: 1rem;
  }
    
  button {
    display: block;
    margin: 2rem auto 0;
  }
`;

const CustomButton = styled(Button)`
  font-size: 24px;
  font-weight: 300;
  line-height: 27.58px;
`;

export function AccessForm(props: { isLogin: boolean }) {
  const { isLogin } = props
  const { handleSignIn } = useAuthorization()
  const navigate = useNavigate()
  const formRef = useRef(null)
  const [submitted, setSubmitted] = useState(false) 

  function handleAccess(data:object, { reset }:any) {
    const { isRegistered, parsedRegistration } = registeredUser(data)
    
    if (!data.name && !isLogin) formRef.current.setFieldError('name', 'Campo obrigatório')
    if (!data.email) formRef.current.setFieldError('email', 'Campo obrigatório')
    if (!data.password) formRef.current.setFieldError('password', 'Campo obrigatório')
    if (isRegistered) formRef.current.setFieldError('email', 'E-mail já cadastrado')

    if (isLogin) {
      if (data.email !== isRegistered?.email || data.password !== isRegistered?.password) {
        formRef.current.setFieldError('email', 'Preencha corretamente')
        formRef.current.setFieldError('password', 'Preencha corretamente')
        return
      }
      handleSignIn(data, isRegistered, navigate)
    } else {
      setSubmitted(handleSignUp(data, reset, parsedRegistration, isRegistered))
    }
  }
  
  return (
    submitted
      ? (
        <h2>Sucesso.</h2>
      ) : (
        <CustomForm ref={formRef} onSubmit={handleAccess}>
          {!isLogin && <FormField
            icon={{
              image: 'user',
              proportion: '2x',
              title: 'Usuário',
              variation: 'lighttext'
            }}
            input={{
              name: 'name',
              placeholder: 'Nome',
              type: 'text'
            }}
          />}
          <FormField
            icon={{
              image:'email',
              proportion: '2x',
              title: 'E-mail',
              variation: 'lighttext'
            }}
            input={{
              name: 'email',
              placeholder: 'E-Mail',
              type: 'email'
            }}
          />
          <FormField
            icon={{
              image:'lock',
              proportion: '2x',
              variation: 'lighttext'
            }}
            input={{
              name: 'password',
              placeholder: 'Senha',
              type: 'password'
            }}
          />
          <CustomButton
            className='custom-button'
            type='submit'>
              {location ? 'Entrar' : 'Cadastrar'}
            </CustomButton>
        </CustomForm>
      )
  )
}