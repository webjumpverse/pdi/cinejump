import { Link, useLocation } from 'react-router-dom'

import styled from 'styled-components'

import { DESIGN_SYSTEM as DS } from '../ds'
import { AccessForm } from '../organisms/forms'

const CustomContainer = styled.div`
  color: ${DS.COLORS.light};
  display: flex;
  flex-direction: column;

  @media screen and (min-width:1024px) {
    display: grid;
    flex: 1;
  }

  h2 {
    font-size: ${DS.FONTS.size.large};
    font-weight: ${DS.FONTS.weight['semi-bold']};
    margin-bottom: 32px;
    
    @media screen and (min-width:1024px) {
      font-size: ${DS.FONTS.size['extra-large']};
      margin-bottom: 64px;
    }
  }

  p {
    font-size: ${DS.FONTS.size.medium};
    font-weight: ${DS.FONTS.weight.thin};
    line-height: 32px;
    margin-bottom: 32px;

    @media screen and (min-width:1024px) {
      font-size: 30px;
      font-weight: 300;
      line-height: 48px;
      margin-bottom: 64px;
    }
  }

  &.login {
    grid-template-columns: minmax(auto, 70%) minmax(550px, 30%);
  }
  
  &.signup {
    @media screen and (min-width:1024px) {
      grid-template-columns: minmax(550px, 30%) minmax(auto, 70%);
    }
  }

  .form,
  .greetings {
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-height: 50vh;
    padding: ${DS.PADDINGS.regular};

    @media screen and (min-width:1024px) {
      padding: ${DS.PADDINGS.large};
    }
  }

  .form {
    color: ${DS.COLORS.primary};
  }

  .greetings {
    background-color: ${DS.COLORS.primary};
    text-align: center;

    a {
      border: ${DS.BORDERS.line.regular} solid ${DS.COLORS.white};
      border-radius: 9999px;
      color: inherit;
      display: block;
      font-size: ${DS.FONTS.size.medium};
      font-weight: 300;
      line-height: 27.58px;
      max-width: 22.5rem;
      padding: .5rem .75rem;
      text-decoration: inherit;
      text-transform: uppercase;
      width: 100%;

      @media screen and (min-width:1024px) {
        border: ${DS.BORDERS.line.big} solid ${DS.COLORS.white};
        font-size: 24px;
        font-weight: 300;
        line-height: 27.58px;
        padding: 1rem 1.5rem;
      }
    }
  }
`;

export function Access() {
  const { pathname } = useLocation()
  const location = pathname.slice(1, pathname.length)
  const loginOrSignup = location === 'login'
  const positions = loginOrSignup ? ['form', 'greetings'] : ['greetings', 'form']

  const DynamicRedirect = (
    <Link to={loginOrSignup ? '/signup' : '/login'}>
      {loginOrSignup ? 'Criar Conta' : 'Login'}
    </Link>
  )
  
  return (
    <CustomContainer className={location}>
      <div className={positions[0]}>
        <h2>{loginOrSignup ? 'Login' : 'Bem-vindo, Jumper!'}</h2>
        {!loginOrSignup && <p>Para se manter conectado, faça login com suas credenciais.</p>}
        {!loginOrSignup && DynamicRedirect}
        {loginOrSignup && <AccessForm isLogin={loginOrSignup} />}
      </div>
      <div className={positions[1]}>
        <h2>{loginOrSignup ? 'Olá, visitante!' : 'Criar Conta'}</h2>
        {loginOrSignup && <p>Cadastre-se e conheça as vantagens do Cinejump.</p>}
        {loginOrSignup && DynamicRedirect}
        {!loginOrSignup && <AccessForm isLogin={loginOrSignup} />}
      </div>
    </CustomContainer>
  )
}