import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Skeleton from 'react-loading-skeleton'

import { FavoriteMovie } from '../molecules/favorite-movie'
import { OthersHighlight } from '../organisms/highlights/others'
import { List } from '../organisms/list'
import { Section } from '../organisms/section'
import { CommonHeader } from '../organisms/common-header'
import { Image } from '../atoms/image'
import { Footer } from '../organisms/footer'

import { getInfosById } from '../../services'

export function Person() {
  const { id } = useParams();
  const [person, setPerson]:any = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getInfosById('person', id)
      .then(person => {
        setPerson(person)
        setLoading(false)
      })
      .catch(error => console.error('person getInfosById error', error));
  }, []);

  return (
    <>
      <CommonHeader page='person' />
      <main>
        {loading ? <Skeleton /> : <OthersHighlight data={person} />}
        <Section title='Conhecido por'>
          {loading ? (
            <Skeleton />
          ) : (
            person.credits.cast.map((movie: any) => <FavoriteMovie key={movie.id} format='poster' movie={movie} />)
          )}
        </Section>
        <Section title='Filmografia' grid={false}>
          {loading ? (
            <Skeleton />
          ) : (
            <List credits={person.credits.cast} />
          )}
        </Section>
        <Section title='Fotos'>
          {loading ? (
            <Skeleton />
          ) : (
            person.images.profiles.map((profile:any, index:number) => <Image key={index} source={`${import.meta.env.VITE_BASE_IMAGE_URL}${profile.file_path}`} format='poster' />)
          )}
        </Section>
      </main>
      <Footer />
    </>
  )
}