import { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import Skeleton from 'react-loading-skeleton'

import { CommonHeader } from '../organisms/common-header'

import { Image } from '../atoms/image'
import { FavoriteMovie } from '../molecules/favorite-movie'
import { ImageLegend } from '../molecules/image-legend'
import { Trailer } from '../molecules/trailer'
import { Footer } from '../organisms/footer'
import { OthersHighlight } from '../organisms/highlights/others'
import { Section } from '../organisms/section'

import { getInfosById } from '../../services'

import 'react-loading-skeleton/dist/skeleton.css'

export function Movie() {
  const { id } = useParams()
  const [movie, setMovie] = useState({})
  const [loading, setLoading] = useState(true);
  const isPerson = false;
  const {
    credits,
    recommendations,
    videos
  }:any = movie;

  useEffect(() => {
    getInfosById('movie', id)
      .then(movie => {
        setMovie(movie)
        setLoading(false)
      })
      .catch(error => console.error('movie getInfosById error', error))
  }, [])

  return (
    <>
      {loading ? <Skeleton /> : <CommonHeader page='movie' background={movie.backdrop_path} />}
      <main>
        {loading ? <Skeleton /> : <OthersHighlight data={movie} isPerson={isPerson} />}
        <Section title='Elenco Principal'>
          {loading ? (
            <Skeleton />
          ) : (
            credits.cast.map((person: any) => (
              <Link to={`/person/${person.id}`} key={person.id}>
                <Image source={`${import.meta.env.VITE_BASE_IMAGE_URL}${person.profile_path}`} format='poster'>
                  <ImageLegend title={person.name} caption={person.character} />
                </Image>
              </Link>
            ))
          )}
        </Section>
        <Section title='Vídeos'>
          {loading ? <Skeleton /> : videos.results.map((video:any) => <Trailer key={video.id} trailer={video} />)}
        </Section>
        <Section title='Recomendações'>
          {loading ? <Skeleton /> : recommendations.results.map((recommendation:any) => <FavoriteMovie key={recommendation.id} format='poster' movie={recommendation} />)}
        </Section>
        <Footer />
      </main>
    </>
  )
}