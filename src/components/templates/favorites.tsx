import { FavoritesList } from '../organisms/favorites-list'
import { Footer } from '../organisms/footer'
import { Section } from '../organisms/section'

import { useFavorites } from '../../contexts'

import { CommonHeader } from '../organisms/common-header'

export function Favorites() {
  const { favorites } = useFavorites()

  return (
    <>
      <CommonHeader page='favorites' />
      <main>
        <Section title='Favoritos' grid={false}>
          {favorites.length
            ? (
              favorites.map(favorite => <FavoritesList key={favorite.id} movie={favorite} />)
            ) : (
              <p>Nenhum filme favoritado.</p>
            )
          }
        </Section>
      </main>
      <Footer />
    </>
  )
}