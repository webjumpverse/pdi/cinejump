import { useEffect, useState } from 'react'

import { HomeHighlights } from '../organisms/highlights/home'
import { Section } from '../organisms/section'
import { Footer } from '../organisms/footer'

import { Trailer } from '../molecules/trailer'
import { FavoriteMovie } from '../molecules/favorite-movie'

import { CommonHeader } from '../organisms/common-header'

import { getMovies, getMoviesTrailers } from '../../services'
import { useFavorites } from '../../contexts'

export function Home() {
  const[topRated, setTopRated]:any[] = useState([])
  const[popular, setPopular]:any[] = useState([])
  const[nowPlaying, setNowPlaying]:any[] = useState([])
  const[upcoming, setUpcoming]:any[] = useState([])
  const[trailers, setTrailers]:any[] = useState([])

  const { favorites } = useFavorites()

  useEffect(() => {
    getMovies('top_rated')
      .then(movies => setTopRated([...movies].slice(0,3)))
      .catch(error => console.error('get top_rated movies effect', error));
  }, []);

  useEffect(() => {
    getMovies('popular')
      .then(movies => setPopular([...movies]))
      .catch(error => console.error('get popular movies effect', error));
  }, []);

  useEffect(() => {
    getMovies('now_playing')
      .then(movies => setNowPlaying([...movies]))
      .catch(error => console.error('get now_playing movies effect', error));
  }, []);

  useEffect(() => {
    getMovies('upcoming')
      .then(movies => setUpcoming([...movies]))
      .catch(error => console.error('get upcoming movies effect', error));
  }, []);

  useEffect(() => {
    getMoviesTrailers(upcoming)
      .then(trailers => setTrailers(trailers))
      .catch(error => console.error('set upcoming movies effect', error));
  }, [upcoming]);

  return (
    <>
      <CommonHeader page='home' />
      <main>
        <HomeHighlights data={topRated} />
        <Section title='Populares'>
          {popular && popular.map((movie:any) => <FavoriteMovie key={movie.id} movie={movie} format='poster' />)}
        </Section>
        <Section title='Em Exibição'>
          {nowPlaying && nowPlaying.map((movie:any) => <FavoriteMovie key={movie.id} movie={movie} format='poster' />)}
        </Section>
        <Section title='Trailers' variation='light'>
          {trailers && trailers.map((trailer:any) => <Trailer key={trailer.id} trailer={trailer} />)}
        </Section>
        <Section title='Favoritos'>
          {favorites.length ? favorites.map((fav:any) => <FavoriteMovie key={fav.id} movie={fav} format='poster' />) : <p>Nenhum filme favoritado.</p>}
        </Section>
      </main>
      <Footer />
    </>
  )
}
