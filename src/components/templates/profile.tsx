import { useRef } from 'react'
import styled from 'styled-components'
import { Form } from '@unform/web'

import { CommonHeader } from '../organisms/common-header'

import { Button } from '../atoms/button'
import { FormField } from '../molecules/form-field'
import { Footer } from '../organisms/footer'

import { DESIGN_SYSTEM as DS } from '../ds'

import { ReactComponent as UserProfile } from '../../assets/imgs/FiUserProfile.svg'
import { useAuthorization } from '../../contexts'

const MainContainer = styled.main`
  background-color: ${DS.COLORS.secondary};
  padding: 0 ${DS.PADDINGS.regular};


`;

const CustomForm = styled(Form)`
  display: flex;
  flex-direction: column;
  gap: 1rem 0;
  margin: 0 auto;
  max-width: 530px;

  button {
    margin: 1rem auto 0;
    max-width: 360px;
  }
`;

const CustomButton = styled(Button)`
  font-size: 24px;
  font-weight: 300;
`;

export function Profile() {
  const { user } = useAuthorization()
  const formRef = useRef(null)
  function handleSubmit(data:any) {
    console.log(data)
  }

  return (
    <>
      <CommonHeader page='profile' />
      <MainContainer>
        <div style={{ display: 'flex', justifyContent: 'center', margin: '0 auto 2rem', maxWidth: '530px'}}>
          <UserProfile />
        </div>
        <CustomForm ref={formRef} onSubmit={handleSubmit}>
          <FormField icon={{ proportion: '2x', variation: 'lighttext', title: 'Usuário', image: 'user' }} input={{name: 'user', placeholder: 'Digite seu usuário.', type: 'text', readOnly: true, value: user.name}} />
          <FormField icon={{ proportion: '2x', variation: 'lighttext', title: 'E-Mail', image: 'email' }} input={{name: 'email', placeholder: 'Digite seu e-mail.', type: 'email', readOnly: true, value: user.email}} />
          <FormField icon={{ proportion: '2x', variation: 'lighttext', title: 'Senha', image: 'lock' }} input={{name: 'password', placeholder: 'Digite a nova senha', type: 'password'}} />
          <FormField icon={{ proportion: '2x', variation: 'lighttext', title: 'Senha', image: 'lock' }} input={{name: 'password', placeholder: 'Confirme a nova senha', type: 'password'}} />
          <CustomButton className='custom-button' type='submit' variation={true}>Atualizar Perfil</CustomButton>
        </CustomForm>
      </MainContainer>
      <Footer />
    </>
  )
}