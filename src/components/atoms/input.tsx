import { useEffect, useRef } from 'react';
import { useField } from '@unform/core';
import styled from 'styled-components'

import { DESIGN_SYSTEM as DS } from '../ds'

export interface InputProps {
  name:string;
}

const CustomInput = styled.input`
  background-color: inherit;
  border: ${DS.BORDERS.line.none};
  color: ${DS.COLORS.lighttext};
  font-family: inherit;
  font-size: ${DS.FONTS.size['semi-medium']};
  font-weight: ${DS.FONTS.weight.thin};
  outline: none;
  width: 100%;

  &::placeholder {
    color: inherit;
  }
`;

export function Input({ name, ...rest }:InputProps) {
  const inputRef = useRef(null)
  const { clearError, defaultValue, error, fieldName, registerField } = useField(name)

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    })
  }, [fieldName, registerField])

  return (
    <div>
      <CustomInput ref={inputRef} defaultValue={defaultValue} onFocus={clearError} {...rest} />
      { error && <span>{error}</span>}
    </div>
  )
}