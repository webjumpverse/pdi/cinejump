import { ReactComponent as FacebookIcon } from '../../assets/imgs/FiFacebook.svg'
import { ReactComponent as HeartIcon } from '../../assets/imgs/FiHeart.svg'
import { ReactComponent as InstagramIcon } from '../../assets/imgs/FiInstagram.svg'
import { ReactComponent as LockIcon } from '../../assets/imgs/FiLock.svg'
import { ReactComponent as LogoutIcon } from '../../assets/imgs/FiLogout.svg'
import { ReactComponent as MailIcon } from '../../assets/imgs/FiMail.svg'
import { ReactComponent as PlayIcon } from '../../assets/imgs/FiPlay.svg'
import { ReactComponent as SearchIcon } from '../../assets/imgs/FiSearch.svg'
import { ReactComponent as TwitterIcon } from '../../assets/imgs/FiTwitter.svg'
import { ReactComponent as UserIcon } from '../../assets/imgs/FiUser.svg'
import { ReactComponent as UserCircleIcon } from '../../assets/imgs/FiUserCircle.svg'
import { DESIGN_SYSTEM as DS } from '../ds'

import { colorScheme } from '../../helpers/variations'

export interface IconProps {
  title: string;
  proportion?: string;
  variation?: string;
  fill?: string;
  image: 'facebook' | 'heart' | 'instagram'  | 'lock'  | 'logout'  | 'email'  | 'play'  | 'search' | 'twitter' | 'user' | 'user-circle';
  isFavorite?: boolean;
}

export function Icon({ title, proportion, variation, fill, image, isFavorite = false }:IconProps) {
  let DynamicIcon;
  let size;

  const favorited = isFavorite ? DS.COLORS.primary : DS.COLORS.black;
  const stroke = colorScheme(variation);
  const strokeCircle = colorScheme(fill);

  switch(proportion) {
    case '2x':
      size = {
        width: 24,
        height: 24,
      }
      break;
    case '3x':
      size = {
        width: 36,
        height: 36,
      }
      break;
    case '4x':
      size = {
        width: 72,
        height: 72,
      }
      break;
    default:
      size = {
        width: 16,
        height: 16,
      }
  }

  switch(image) {
    case 'facebook':
      DynamicIcon = <FacebookIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'heart':
      DynamicIcon = <HeartIcon stroke={variation ? stroke : favorited} fill={fill ? fill : favorited} title={title} width={size.width} height={size.height} />;
      break;
    case 'instagram':
      DynamicIcon = <InstagramIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'lock':
      DynamicIcon = <LockIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'logout':
      DynamicIcon = <LogoutIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'email':
      DynamicIcon = <MailIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'play':
      DynamicIcon = <PlayIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'search':
      DynamicIcon = <SearchIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'twitter':
      DynamicIcon = <TwitterIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'user':
      DynamicIcon = <UserIcon stroke={stroke} title={title} width={size.width} height={size.height} />;
      break;
    case 'user-circle':
      DynamicIcon = <UserCircleIcon fill={strokeCircle} title={title} width={size.width} height={size.height} />;
      break;
    default:
      DynamicIcon = <span>You have to define an icon type</span>
  }
  return (
    DynamicIcon
  );
}