import Skeleton from 'react-loading-skeleton'
import styled from 'styled-components'

import { Icon } from './icon'

import { DESIGN_SYSTEM as DS } from '../ds'

import { getObjectValue } from '../../helpers/objects'

import 'react-loading-skeleton/dist/skeleton.css'

interface ImageProps {
  className?: string;
  source: string | undefined;
  title?: string;
  alt?: string;
  format: string;
  children?: React.ReactElement;
}

const SIZES = {
  modal: [490, 750],
  poster: [166, 254],
  prime: [710, 328],
  profile: [294, 452],
  responsive: ['100%', 'auto'],
  second: [274, 156],
  thumb: [100, 152],
  trailer: [400, 226],
};

const ImageContainer = styled.figure`
  border-radius: ${DS.BORDERS.radius.small};
  display: flex;
  margin: 0;
  min-width: fit-content;
  overflow: hidden;
  position: relative;

  @media screen and (min-width:1024px) {
    min-width: fit-content;
  }
`;

export function Image({ className, source, title, alt, format, children }:ImageProps) {
  const size = getObjectValue(SIZES, format);

  return (
    <ImageContainer className={className}>
      {children}
      {format === 'trailer' && <a href='#' style={{ alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)', display: 'flex', justifyContent: 'center', height: '100%', position: 'absolute', width: '100%' }}><Icon image='play' title='Play' variation='white' proportion='4x' /></a>}
      {!source ? <Skeleton width={size[0]} height={size[1]} /> : <img src={source} title={title} alt={alt} width={size[0]} height={size[1]} />}
    </ImageContainer>
  )
}