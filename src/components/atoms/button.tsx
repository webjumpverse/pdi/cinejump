import styled, { css } from 'styled-components'

import { DESIGN_SYSTEM as DS } from '../ds'

interface ButtonProps {
  className: string;
  children: string | JSX.Element;
  type: 'button' | 'submit' | 'reset';
  variation?: boolean;
}

const CustomButton = styled.button<ButtonProps>`
  background-color: ${DS.COLORS.primary};
  border: ${DS.BORDERS.line.none};
  border-radius: ${DS.BORDERS.rounded};
  color: ${DS.COLORS.white};
  font-family: inherit;
  max-width: 22.5rem;
  padding: ${DS.PADDINGS.regular} ${DS.PADDINGS['semi-medium']};
  text-transform: uppercase;
  width: 100%;

  ${props => props.variation && css`
    background-color: ${DS.COLORS.none};
    border: ${DS.BORDERS.line.big} solid ${DS.COLORS.white};
  `}
`;

export function Button({ type, variation = false, className, children }:ButtonProps) {
  return (
    <CustomButton className={className} type={type} variation={variation}>{children}</CustomButton>
  );
}