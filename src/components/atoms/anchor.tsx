import { Link } from 'react-router-dom';
import styled from 'styled-components'

import { DESIGN_SYSTEM as DS } from '../ds'

const CustomLink = styled(Link)`
  color: ${DS.COLORS.light};
  display: block;
  font-weight: ${DS.FONTS.weight.thin};
  text-decoration: none;

  &:not(:last-child) {
    margin-bottom: 1rem;
  }
`;

export function Anchor({ className, children, to }:any) {
  return (
    <CustomLink className={className} to={to}>{children}</CustomLink>
  )
}