import { colorScheme } from '../../helpers/variations'

import { DESIGN_SYSTEM as DS } from '../ds'

interface TitlesProps {
  type?: string;
  variation?: string;
  children: React.ReactNode;
}

export function Title({ type, variation, children }:TitlesProps) {
  const color = colorScheme(variation);

  const STYLE = {
     color: color,
     margin: 0,
  }

  let DynamicHead;
  switch (type) {
    case 'h2':
      DynamicHead = <h2 style={{
        ...STYLE,
        fontSize: `${DS.FONTS.size.medium}`,
        fontWeight: `${DS.FONTS.weight.thin}`,
      }}>{children}</h2>
      break;
    case 'h3':
      DynamicHead = <h3 style={{
        ...STYLE,
        fontSize: `${DS.FONTS.size['semi-medium']}`,
        fontWeight: `${DS.FONTS.weight['semi-bold']}`,
      }}>{children}</h3>
      break;
    default:
      DynamicHead = <h1 style={{
        ...STYLE,
        fontSize: `${DS.FONTS.size.big}`,
        fontWeight: `${DS.FONTS.weight['semi-bold']}`,
      }}>{children}</h1>
  }

  return (
    DynamicHead
  )
}