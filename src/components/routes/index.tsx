import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
} from 'react-router-dom'

import { Favorites, Home, Access, Movie, Person, Profile } from '../templates'
import ErrorPage from '../../error-page'
import { useAuthorization } from '../../contexts'

const ProtectedRoute = (props: { blocked:boolean, children:JSX.Element }) => {
  const { blocked, children } = props

  if (!blocked) {
    return <Navigate to='/login' replace />
  }

  return children
}

export function Routers() {
  const { authorization } = useAuthorization()

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<ProtectedRoute blocked={authorization}><Home /></ProtectedRoute>} errorElement={<ErrorPage />} />
        <Route path='signup' element={<Access />} errorElement={<ErrorPage />} />
        <Route path='login' element={<Access />} errorElement={<ErrorPage />} />
        <Route path='movie/:id' element={<ProtectedRoute blocked={authorization}><Movie /></ProtectedRoute>} errorElement={<ErrorPage />} />
        <Route path='person/:id' element={<ProtectedRoute blocked={authorization}><Person /></ProtectedRoute>} errorElement={<ErrorPage />} />
        <Route path='favorites' element={<ProtectedRoute blocked={authorization}><Favorites /></ProtectedRoute>} errorElement={<ErrorPage />} />
        <Route path='profile' element={<ProtectedRoute blocked={authorization}><Profile /></ProtectedRoute>} errorElement={<ErrorPage />} />
      </Routes>
    </BrowserRouter>
  )
}