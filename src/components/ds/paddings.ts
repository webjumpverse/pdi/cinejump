export const PADDINGS = {
  'semi-small': '0.125rem', // 2px
  small: '0.25rem', // 4px
  'semi-regular': '0.5rem', // 8px
  regular: '1rem', // 16px
  'semi-medium': '1.5rem', // 24px
  medium: '2rem', // 32px
  'semi-big': '2.5rem', // 40px
  big: '3rem', // 48px
  'semi-large': '3.5rem', // 40px
  large: '4rem', // 64px
}