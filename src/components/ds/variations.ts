import { DESIGN_SYSTEM as DS } from '.'

export function colorScheme(variation:string|undefined) {
  let theme;
  switch(variation) {
    case 'primary':
      theme = DS.COLORS.primary;
      break;
    case 'secondary':
      theme = DS.COLORS.secondary;
      break;
    case 'light':
      theme = DS.COLORS.light;
      break;
    case 'lighttext':
      theme = DS.COLORS.lighttext;
      break;
    default: 
      theme = 'none';
  }

  return theme;
}