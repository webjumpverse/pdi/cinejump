export const FONTS = {
  family: 'Ubuntu',
  size: {
    'x-small': '0.5rem',
    small: '0.75rem', 
    regular: '1rem',
    'semi-medium': '1.25rem',
    medium: '1.5rem',
    'semi-big': '1.75rem',
    big: '2rem',
    large: '2.25rem', 
    'extra-large': '4rem'
  },
  weight: {
    thin: 300,
    normal: 400,
    'semi-bold': 500,
  }
}