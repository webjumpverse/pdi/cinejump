export const COLORS = {
  primary: '#E83F5B',
  secondary: '#80BCB8',
  light:' #F9F9F9',
  lighttext: '#C0C0C0',
  lightinputbackgroung: '#EFEFEF',
  white: '#FFFFFF',
  black: '#000000',
  none: 'transparent',
}