export const MARGINS = {
  none: '0',
  small: '0.5rem',
  regular: '1rem',
  medium: '2rem',
  big: '1.5rem',
  larg: '3.5rem'
}