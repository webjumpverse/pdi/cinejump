import { BORDERS } from './borders'
import { COLORS } from './colors'
import { FONTS } from './fonts'
import { MARGINS } from './margins'
import { PADDINGS } from './paddings'

export const DESIGN_SYSTEM = {
  BORDERS,
  COLORS,
  FONTS,
  MARGINS,
  PADDINGS
}