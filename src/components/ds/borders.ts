export const BORDERS = {
  line: {
    none: '0',
    small: '1px',
    regular: '2px',
    big: '3px'
  },
  radius: {
    small: '0.75rem',
    regular: '1.25rem',
    big: '3rem'
  },
  rounded: '9999px',
}