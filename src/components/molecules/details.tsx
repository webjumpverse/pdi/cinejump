import styled from 'styled-components'

import { Title } from '../atoms/title'

import { DESIGN_SYSTEM as DS } from '../ds';

const Release = styled.p`
  margin: ${DS.MARGINS.small} 0 ${DS.MARGINS.regular};
`;
const Overview = styled.p`
  line-height: 2rem;
`;

export function Details({ movie }:any) {
  const {
    overview,
    release_date,
    title,
  } = movie;

  const movieYear = release_date.split('-').slice(0,1)
  return (
    <div>
      <Title type='h3' variation='primary'>{title}</Title>
      <Release>{movieYear}</Release>
      <Overview>{overview}</Overview>
    </div>
  )
}