import styled from 'styled-components'

import { Icon } from '../atoms/icon'
import { Anchor } from '../atoms/anchor'

import { DESIGN_SYSTEM as DS } from '../ds'

import { HeaderProps } from '../organisms/common-header'

import { ReactComponent as RedLogo } from '../../assets/imgs/RedLogo.svg'
import { ReactComponent as WhiteLogo } from '../../assets/imgs/WhiteLogo.svg'

const CustomNav = styled.nav`
  padding: 1rem 0;
`;

const CustomMenu = styled.ul`
  display: grid;
  gap: 1rem 0;
  grid-template-areas:
    'logo logo logo logo'
    'item1 item2 item3 item4';

  @media screen and (min-width:1024px) {
    display: flex;
    align-items: center;
  }

  li {
    display: flex;

    &:not(:last-child) {
      margin-right: ${DS.MARGINS.small};
    }

    @media screen and (min-width:1024px) {
      min-width: 5rem;
    }

    &.logo {
      grid-area: logo;
      justify-content: center;

      @media screen and (min-width:1024px) {
        display: inline-flex;
        width: 100%;
      }
    }
  }
`;

export function Menu({ page }:HeaderProps) {
  return (
    <CustomNav className={`${page == 'favorites' ? 'favorites' : ''} container`}>
      <CustomMenu>
        <li><Anchor to='/'>Filmes</Anchor></li>
        <li><Anchor to='#'>Séries</Anchor></li>
        <li className='logo'>{page == 'favorites' ? <RedLogo /> : <WhiteLogo />}</li>
        <li><Icon image='search' title='Search' variation={`${page == 'favorites' ? 'primary' : 'light'}`} /></li>
        <li><Icon image='user-circle' title='Search' fill={`${page == 'favorites' ? 'primary' : 'light'}`} /></li>
      </CustomMenu>
    </CustomNav>
  )
}