import styled from 'styled-components'

import { Icon } from '../atoms/icon'

const SocialMediasContainer = styled.div`
  display: flex;
  gap: 0 1rem;
  margin-bottom: 2rem;
`;

export function SocialMedias({ data }:any) {
  const isMobile = globalThis.innerWidth < 1024;
  return (
    <SocialMediasContainer>
      <Icon image='instagram' title='instagram' proportion='3x' variation={isMobile ? 'lighttext' : 'light'} />
      <Icon image='twitter' title='twitter' proportion='3x' variation={isMobile ? 'lighttext' : 'light'} />
      <Icon image='facebook' title='facebook' proportion='3x' variation={isMobile ? 'lighttext' : 'light'} />
    </SocialMediasContainer>
  )
}