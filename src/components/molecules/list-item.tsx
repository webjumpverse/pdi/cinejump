import styled from 'styled-components'
import { descCredits } from '../../helpers/order'

import { DESIGN_SYSTEM as DS } from '../ds'

interface ListItemsProps {
  items: any[]
}

const CustomItem = styled.li`
  border-bottom: ${DS.BORDERS.line.regular} solid ${DS.COLORS.lightinputbackgroung};
  color: ${DS.COLORS.lighttext};
  padding: ${DS.PADDINGS['semi-medium']} 0;
  width: 100%;

  span {
    font-weight: ${DS.FONTS.weight['semi-bold']}
  }
`;

export function ListItems({ items }:ListItemsProps) {
  const credits = items.sort(descCredits).filter(item => item.release_date !== "");
  
  return (
    <>
      {credits.map((item:any) => <CustomItem key={item.id}>{item.release_date.split('-')[0]} - <span>{item.title}</span> como {item.character}</CustomItem>)}
    </>
  )
}