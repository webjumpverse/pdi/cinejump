import React from 'react'
import styled from 'styled-components'

import { Icon } from '../atoms/icon'
import { DESIGN_SYSTEM as DS } from '../ds'

const MenuItemContainer = styled.div`
  display: flex;
  font-weight: ${DS.FONTS.weight.thin};
  gap: 0 ${DS.PADDINGS.regular};
  padding: ${DS.PADDINGS.regular};
  
  p {
    line-height: 20px;
    margin: 0;
  }
`;

const MenuHeader = styled.p`
  color: ${DS.COLORS.secondary};
  font-size: ${DS.FONTS.size['semi-medium']};
`;

export function MenuItem(props: any) {
  const { children, isHeader = false, ...icon} = props;
  return (
    <MenuItemContainer>
      {!isHeader && <Icon {...icon} />}
      {isHeader ? <MenuHeader>{children}</MenuHeader> : <p>{children}</p>}
    </MenuItemContainer>
  );
}