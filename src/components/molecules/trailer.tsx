import styled, { css } from 'styled-components'

import { Image } from '../atoms/image'

import { DESIGN_SYSTEM as DS } from '../ds'

interface TrailerProps {
  isHome?: boolean;
  trailer?: any;
}

const TrailerContainer = styled.div<TrailerProps>`
  ${props => props.isHome && css`
    max-width: 400px;
  `}

  figure {
    margin-bottom: ${DS.MARGINS.small};
  }
  p {
    color: ${props => props.isHome ? DS.COLORS.light : DS.COLORS.lighttext};
    font-weight: ${DS.FONTS.weight.thin};
  }
`;

export function Trailer({ trailer }:TrailerProps) {
  const {
    key,
    name
  } = trailer;
  const isHome = true;
  
  return (
    <TrailerContainer isHome={isHome}>
      <Image source={`https://img.youtube.com/vi/${key}/maxresdefault.jpg`} format='trailer' />
      <p>{name}</p>
    </TrailerContainer>
  )
}