import styled, { css } from 'styled-components'

import { DESIGN_SYSTEM as DS } from '../ds'

interface ImageLegendProps {
  isPrime?: boolean;
  variation?: boolean;
  title?: string;
  caption?: string;
}

const CustomImageLegend = styled.div<ImageLegendProps>`
  background-color: ${DS.COLORS.light};
  padding: ${DS.PADDINGS.regular} ${DS.PADDINGS.small};
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;

  p {
    margin: ${DS.MARGINS.none};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .title {
    color: ${DS.COLORS.secondary};
    font-size: ${DS.FONTS.size.regular};
    font-weight: ${DS.FONTS.weight.normal};
    margin-bottom: ${DS.MARGINS.small};
  }
  .text {
    color: ${DS.COLORS.lighttext};
    font-size: ${DS.FONTS.size.small};
    font-weight: ${DS.FONTS.weight.thin};
    line-height: 1.125rem;
  }


  ${props => props.variation && css`
    background-color: rgba(0, 0, 0, 0.3);
    color: ${DS.COLORS.lightinputbackgroung};
    padding: ${DS.PADDINGS.regular};

    p {
      overflow: unset;
      text-overflow: unset;
      white-space: unset;
    }

    .title {
      color: inherit;
      font-size: ${DS.FONTS.size.medium};
      font-weight: ${DS.FONTS.weight.normal};
      margin-bottom: unset;
    }
    .text {
      color: inherit;
      font-size: ${DS.FONTS.size.regular};
      font-weight: ${DS.FONTS.weight.thin};
      line-height: 1.5rem;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box !important;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      white-space: normal;
    }
  `}

  ${props => !props.isPrime && css`
    padding: ${DS.PADDINGS.small};
  `}
`;

export function ImageLegend({ isPrime = false, variation = false, title, caption }:ImageLegendProps) {
  return (
    <CustomImageLegend variation={variation} isPrime={isPrime}>
      <p className='title'>{title}</p>
      {caption && <p className='text'>{caption}</p>}
    </CustomImageLegend>
  )
}