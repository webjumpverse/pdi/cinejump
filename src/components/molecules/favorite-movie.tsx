import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { Image } from '../atoms/image'
import { Icon } from '../atoms/icon'

import { DESIGN_SYSTEM as DS } from '../ds'

import { useFavorites } from '../../contexts'

interface FavoriteMovieProps {
  movie: any;
  format: string;
}

const CustomIcon = styled.button`
  background-color: ${DS.COLORS.none};
  border: ${DS.BORDERS.line.none};
  cursor: pointer;
  height: 1rem;
  padding: 0;
  position: absolute;
  top: 6px;
  right: 6px;
`;

export function FavoriteMovie({ movie, format }:FavoriteMovieProps) {
  const { favorites, handleFavorite, isFavorite } = useFavorites();
  const {
    id,
    poster_path,
    title
  } = movie;

  const alreadyAdded = isFavorite(favorites, movie);

  return (
    <div style={{ position: 'relative' }}>
      <Link to={`/movie/${id}`}>
        <Image source={`${import.meta.env.VITE_BASE_IMAGE_URL}${poster_path}`} format={format} />
      </Link>
      <CustomIcon onClick={() => handleFavorite(movie)}><Icon title={title} image='heart' isFavorite={alreadyAdded} /></CustomIcon>
    </div>
  )
}