import styled from 'styled-components'

import { Icon, IconProps } from '../atoms/icon'
import { Input, InputProps } from '../atoms/input'

import { DESIGN_SYSTEM as DS } from '../ds'

interface FormFieldProps {
  icon: any;
  input: any;
  legend?: string;
}

const CustomFieldset = styled.fieldset`
  background-color: ${DS.COLORS.lightinputbackgroung};
  border: ${DS.BORDERS.line.none};
  border-radius: ${DS.BORDERS.radius.small};
  display: flex;
  gap: 0 1rem;
  padding: ${DS.PADDINGS.regular};

  @media screen and (min-width: 1024px) {
    padding: ${DS.PADDINGS['semi-medium']};
  }
`;

export function FormField({ icon, legend, input }: FormFieldProps) {
  return (
    <CustomFieldset>
      {legend && <legend>{legend}</legend>}
      <Icon {...icon} />
      <Input {...input} />
    </CustomFieldset>
  )
}