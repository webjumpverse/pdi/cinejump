import styled from 'styled-components'

import { DESIGN_SYSTEM as DS } from './components/ds'
import { Routers } from './components/routes';
import { FavoritesProvider, AuthorizationProvider } from './contexts'

const Container = styled.div`
  color: ${DS.COLORS.lighttext};
  font-family: ${DS.FONTS.family};
  font-size: ${DS.FONTS.size.regular};
  position: relative;

  .container {
    margin: 0 auto;
    max-width: 62.5rem;
    padding: 0 1rem;
    
    @media screen and (min-width:1024px) {
      padding: 0;
    }
  }

  .bg-primary {
    background-color: ${DS.COLORS.primary};
  }

  .bg-secondary {
    background-color: ${DS.COLORS.secondary};
  }

  .vertical-padding {
    padding: ${DS.PADDINGS.regular} 0;
  }

  .home {
    background-color: ${DS.COLORS.primary};
    height: 376px;
    position: absolute;
    width: 100%;
    z-index: -1;
  }
  
  .favorites {
    * {
      color: ${DS.COLORS.primary} !important;
    }
  }


`;

function App() {
  return (
    <Container>
      <AuthorizationProvider>
        <FavoritesProvider>
          <Routers />
        </FavoritesProvider>
      </AuthorizationProvider>
    </Container>
  );
}

export default App;
