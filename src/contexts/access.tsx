import { createContext, useContext, useEffect, useState } from 'react'
import { getLoggedUser, registeredUser } from '../helpers/access';

interface AuthorizationContextProps {
  authorization: boolean;
  user: object;
  handleSignIn(user:object, registered:object, redirect:Function): void;
}

export const AuthorizationContext = createContext<AuthorizationContextProps>({} as AuthorizationContextProps);

export function AuthorizationProvider({ children }:any) {
  const [user, setUser] = useState<object>({})
  const logged = getLoggedUser('signed-in')

  function handleSignIn(data:object, registered:object, redirect:Function) {
    const { email, password } = data    
    if (!email || !password || !registered) return
    setUser(registered)
    sessionStorage.setItem('signed-in', `${email}`)
    redirect('/')
  }

  useEffect(() => {
    if (!!logged) {
      const { isRegistered } = registeredUser({ email: logged })
      setUser(isRegistered)
    }
  }, [logged])
  
  return (
    <AuthorizationContext.Provider value={{ authorization: !!logged, user, handleSignIn }}>
      { children }
    </AuthorizationContext.Provider>
  )
}

export function useAuthorization() {
  const context = useContext(AuthorizationContext)
  return context
}