import { createContext, useContext, useEffect, useState } from 'react'
import { getObjectValue } from '../helpers/objects'

interface FavoritesContextProps {
  favorites: any[];
  handleFavorite: (id:number) => void;
  isFavorite: (list:any[],item:any) => boolean;
}

export const FavoritesContext = createContext<FavoritesContextProps>({} as FavoritesContextProps);

export function FavoritesProvider({ children }:any) {
  const myFavorites = localStorage.getItem('favorites')
  const parsedFavorites = myFavorites ? JSON.parse(myFavorites) : []
  const [favorites, setFavorites]:any[] = useState(parsedFavorites)

  useEffect(() => {
    if (!favorites.length) {
      localStorage.removeItem('favorites')
    }
  }, [favorites])

  function isFavorite(list:any[],item:any) {
    return list.some((el:any) => getObjectValue(el, 'id') === item.id)
  }

  function handleFavorite(movie:any) {
    const alreadyAdded = isFavorite(favorites,movie)
    
    if (!alreadyAdded) {
      setFavorites([ ...favorites, movie ])    
      localStorage.setItem('favorites', JSON.stringify([ ...favorites, movie ]))
    } else {
      const updatedFavorites = favorites.filter(((fav:any) => fav.id !== movie.id))
      setFavorites(updatedFavorites)    
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites))
    }
  }

  return (
    <FavoritesContext.Provider value={{ favorites, handleFavorite, isFavorite }}>
      { children }
    </FavoritesContext.Provider>
  )
}

export function useFavorites() {
  const context = useContext(FavoritesContext);
  return context;
}